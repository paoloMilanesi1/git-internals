package core;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class GitRepository {
	
	private String path;
	
	public GitRepository(String repoPath) {
		this.path=repoPath;
	}

	public String getHeadRef() throws IOException {
		BufferedReader bf=new BufferedReader(new FileReader(path+"/HEAD"));
		String ret= bf.readLine().substring(5);
		bf.close();
		return ret;
	}

	public String getRefHash(String fileName) throws IOException {
		BufferedReader bf=new BufferedReader(new FileReader(path+"/"+fileName));
		String ret= bf.readLine();
		bf.close();
		return ret;
	}

}
